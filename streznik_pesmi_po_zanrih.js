// Za združljivost razvoja na lokalnem računalniku ali v Cloud9 okolju
if (!process.env.PORT) {
  process.env.PORT = 8080;
}

var express = require('express');
var streznik = express();

var sqlite3 = require("sqlite3").verbose();
var pb = new sqlite3.Database("Chinook.sl3");

var vrniVseZanre = function(callback) {
  pb.all("SELECT * FROM Genre", function(napaka, vrstice) {
    var rezultat = 'Prišlo je do napake!';
    if (!napaka) {
      rezultat = '<h1>Žanri</h1><ul>';
      vrstice.forEach(function (vrstica) {
        rezultat += '<li><a href="/kategorija/' + vrstica.GenreId + '">' + vrstica.Name + '</a></li>';
      });
      rezultat += '</ul>';
    }
    callback(rezultat);
  });
}

var vrniPesmiZanra = function(idZanra, callback) {
  pb.all("SELECT * FROM Track WHERE GenreId = $id", {$id: idZanra}, function(napaka, vrstice) {
    var rezultat = 'Prišlo je do napake!';
    if (!napaka) {
      rezultat = '<h2>Pesmi</h2><ul>';
      vrstice.forEach(function (vrstica) {
        rezultat += '<li><b>' + vrstica.Name + '</b> @ $' + vrstica.UnitPrice + '</li>';
      });
      rezultat += '</ul>';
    }
    callback(rezultat);
  });
}

var vrniPesmiInAvtorjeZanra = function(idZanra, callback) {
  pb.all("SELECT Track.Name AS pesem, Artist.Name AS izvajalec, Track.UnitPrice AS cena \
          FROM Track, Album, Artist \
          WHERE Track.AlbumId = Album.AlbumId AND \
                Album.ArtistId = Artist.ArtistID AND \
                GenreId = $id", {$id: idZanra}, function(napaka, vrstice) {
    var rezultat = 'Prišlo je do napake!';
    if (!napaka) {
      rezultat = '<h2>Pesmi</h2><ul>';
      vrstice.forEach(function (vrstica) {
        rezultat += '<li><b>' + vrstica.pesem + '</b> (' + vrstica.izvajalec +
                    ') @ $' + vrstica.cena + '</li>';
      });
      rezultat += '</ul>';
    }
    callback(rezultat);
  });
}

streznik.get('/', function(zahteva, odgovor) {
  vrniVseZanre(function(rezultat) {
    odgovor.send(rezultat);
  });
})

streznik.get('/kategorija/:idKategorije', function (zahteva, odgovor) {
  vrniVseZanre(function(rezultatKategorije) {
    vrniPesmiInAvtorjeZanra(zahteva.params.idKategorije, function(rezultatIzdelkiKategorije) {
      odgovor.send(rezultatKategorije + rezultatIzdelkiKategorije);
    })
  });
});

streznik.listen(process.env.PORT, function() {
  console.log('Strežnik je pognan!');
})
