// Za združljivost razvoja na lokalnem računalniku ali v Cloud9 okolju
if (!process.env.PORT) {
  process.env.PORT = 8080;
}

var http = require('http');

var streznik = http.createServer(function (zahteva, odgovor) {
  odgovor.writeHead(200, {'Content-Type': 'text/plain'});
  odgovor.end('Lepo pozdravljeni ljubitelji predmeta OIS!');
})

streznik.listen(process.env.PORT, function() {
  console.log('Strežnik je pognan.');
});
